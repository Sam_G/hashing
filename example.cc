#include <iostream>
#include <vector>

#include "sha512.h"
#include "test/test_utils.hh"

int main(){
    NISTVectorParser tests("test/shabytetestvectors/SHA512ShortMsg.rsp", 128);

    for(size_t i = 0; i < tests.test_vec.size(); i++){
        std::vector<uint8_t> input_vec = tests.test_vec.at(i);
        std::vector<uint8_t> ref_vec = tests.hashes.at(i);

        uint8_t* output = (uint8_t*)malloc(sizeof(uint8_t) * 64);
        sha512(input_vec.data(), input_vec.size(), output);

        std::vector<uint8_t> output_vec(output, output + 64);
        for(size_t j = 0; j < 64; j++){
            printf("%02x", output[j]);
        }
        printf("\n");
        free(output);
    }
}