# Hashing

A set of simple, easy to use implementations of standard cryptography algorithms written in C. All code is licensed under the permissive MIT license unless otherwise stated. Currently supported algorithms are:

- SHA256
- SHA512

## Testing

To run the google tests ensure you have gtest installed. The Makefile expects it to be installed in /home/sam/Software/, which is unlikely, so some Makefile edits will be required.
