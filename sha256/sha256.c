#include "sha256.h"

#ifdef __cplusplus
extern "C" {
#endif
struct Message{
    uint8_t *message;
    size_t len;
};

static struct Message* prepare_message(uint8_t* message, size_t len){

    const long len_bits = len * 8;
    size_t zero_pad_len = 511 - 8 * ((len + 8) % 64);
    
    const size_t output_len_bits = len_bits + 1 + zero_pad_len + 64;
    struct Message* output = (struct Message*) malloc(sizeof(struct Message));

    if(output == NULL){
        return NULL;
    }
    
    output->len = output_len_bits / 8;
    output->message = (uint8_t*) malloc(sizeof(uint8_t) * output->len);
    
    if(output->message == NULL){
        free(output);
        return NULL;
    }

    // Copy contents of message to output
    for(unsigned long i = 0; i < len; i++){
        *(output->message + i) = *(message + i);
    }

    // Append  '1' to the output
    *(output->message + len) |= 1UL << 7;

    if(zero_pad_len <= 7){
        for(unsigned long i = 0; i < zero_pad_len; i++){
            *(output->message + len) &= ~(1UL << i);
        }
    } else {
        //Fill up first byte
        for(unsigned long i = 0; i < 7; i++){
            *(output->message + len) &= ~(1UL << i);
        }

        for(unsigned long i = 0; i < (zero_pad_len - 7)/8; i++){
            *(output->message + len + 1 + i) = 0x0;
        }

        for(unsigned long i = 0; i < (zero_pad_len - 7) % 8; i++){
            *(output->message + len + zero_pad_len/8) &= ~(1UL << i);
        }
    }

    uint8_t *len_bits_ptr = (uint8_t*)(&len_bits);
    
    for(int i = 0; i < 8; i++){
        *(output->message + output->len - 8 + i) = *(len_bits_ptr + 7 - i);
    }
    
    return output;
}

void sha256(uint8_t* message, size_t len, uint8_t* buf){

    // Constants [4.2.2]
    const uint32_t K[64] = 
    {0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
    0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
    0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
    0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
    0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
    0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
    0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
    0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2};

    // Initial Hash Value [5.2.3]
    uint32_t H[8] =
    {0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19};

    struct Message* M = prepare_message(message, len);
    
    if(M == NULL){
        // Need to improve error handling here!
        return;
    }

    for(unsigned long i = 0; i < M->len / 64; i++){
        uint8_t *Mi = (M->message + 64*i);
        uint32_t W[64];

        for(int t = 0, j = 0; t < 16; t++, j += 4){
            W[t] = (*(Mi + j) << 24) | (*(Mi + j + 1) << 16) | (*(Mi + j + 2) << 8) | (*(Mi + j + 3));
        }
        for(int t = 16; t < 64; t++){
            W[t] = (SIG1_256(W[t-2]) + W[t-7] + SIG0_256(W[t-15]) + W[t-16]);
        }

        uint32_t a = H[0], b = H[1], c = H[2], d = H[3], e = H[4], f = H[5], g = H[6], h = H[7];

        for(int t = 0; t < 64; t++){
            const uint32_t T1 = (h + EP1_256(e) + CHOICE_256(e,f,g) + K[t] + W[t]);
            const uint32_t T2 = (EP0_256(a) + MAJOR_256(a,b,c));
            h = g;
            g = f;
            f = e;
            e = (d + T1);
            d = c;
            c = b;
            b = a;
            a = (T1 + T2);
        }

        H[0] = (a + H[0]);
        H[1] = (b + H[1]);
        H[2] = (c + H[2]);
        H[3] = (d + H[3]);
        H[4] = (e + H[4]);
        H[5] = (f + H[5]);
        H[6] = (g + H[6]);
        H[7] = (h + H[7]);
    }

    for(int i = 0, j = 0; i < 8; i++){
        *(buf + j++) = (uint8_t)(H[i] >> 24);
        *(buf + j++) = (uint8_t)(H[i] >> 16);
        *(buf + j++) = (uint8_t)(H[i] >> 8);
        *(buf + j++) = (uint8_t)(H[i]);
    }
    free(M->message);
    free(M);
}

#ifdef __cplusplus
}
#endif