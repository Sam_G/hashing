#include <stdlib.h>
#include <stdint.h>

#define ROTL_256(a,b) (((a) << (b)) | ((a) >> (32-(b))))
#define ROTR_256(a,b) (((a) >> (b)) | ((a) << (32-(b))))

#define EP0_256(x) (ROTR_256(x,2) ^ ROTR_256(x,13) ^ ROTR_256(x,22))
#define EP1_256(x) (ROTR_256(x,6) ^ ROTR_256(x,11) ^ ROTR_256(x,25))
#define SIG0_256(x) (ROTR_256(x,7) ^ ROTR_256(x,18) ^ ((x) >> 3))
#define SIG1_256(x) (ROTR_256(x,17) ^ ROTR_256(x,19) ^ ((x) >> 10))
#define CHOICE_256(x,y,z) (((x) & (y)) ^ ((~x) & (z)))
#define MAJOR_256(x,y,z) (((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)))

#ifdef __cplusplus
extern "C" {
#endif
void sha256(uint8_t* message, size_t len, uint8_t* buf);
#ifdef __cplusplus
}
#endif