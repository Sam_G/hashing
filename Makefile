all: example

example: sha256.o sha512.o
	g++ -g -Wall -o example sha256.o sha512.o example.cc -Isha256/ -Isha512/
	rm *.o

tests: sha256.o sha512.o
	g++ -g -Wall -pthread -o test/test sha256.o sha512.o test/test.cc \
		~/Software/lib/libgtest.a ~/Software/lib/libgtest_main.a \
		-Isha256/ -Isha512/ -I/home/sam/Software/include/
	rm *.o

sha256.o:
	gcc -g -Wall -c -o sha256.o sha256/sha256.c

sha512.o:
	gcc -g -Wall -c -o sha512.o sha512/sha512.c

.PHONY: clean
clean:
	rm -f test/test *.o

.PHONY: scan
scan:
	scan-build -V gcc -c sha256/sha256.c sha512/sha512.c -Isha256/ -Isha512/