#include <fstream>
#include <vector>
#include <sstream>
#include <cstring>
#include <string>

class NISTVectorParser{
    void convert_hexstring(char* hex, std::vector<std::vector<uint8_t>> *output){
        std::string hex_str(hex);


        // Convert hex string to bytes
        std::vector<uint8_t> bytes;

        for(size_t i = 0; i < hex_str.length(); i+=2){
            std::string byte_string = hex_str.substr(i, 2);
            uint8_t byte = (uint8_t) strtol(byte_string.c_str(), NULL, 16);
            bytes.push_back(byte);
        }

        output->push_back(bytes);
    }

    public:

    std::vector<std::vector<uint8_t>> test_vec;
    std::vector<std::vector<uint8_t>> hashes;

    NISTVectorParser(std::string filename, size_t hash_len){
        std::ifstream infile(filename);
        std::string line;

        // Get line from file
        while(std::getline(infile, line)){
            // Check if line starts with #, if so skip
            if(line[0] == '#' ) continue;

            if(line[0] == '[' && line[1] == 'L'){
                continue;
            }

            if(line[0] == 'L'){
                size_t test_len;
                sscanf(line.c_str(), "Len = %lu\n", &test_len);
                std::getline(infile, line);
                
                if(test_len != 0){
                    char *hex = (char*) malloc(sizeof(char) * test_len);
                    sscanf(line.c_str(), "Msg = %s\n", hex);
                    convert_hexstring(hex, &this->test_vec);
                    free(hex);                
                } else {
                    std::vector<uint8_t> vec;
                    this->test_vec.push_back(vec);
                }

                std::getline(infile, line);
                char *hex = (char*) malloc(sizeof(char) * hash_len);
                sscanf(line.c_str(), "MD = %s\n", hex);
                convert_hexstring(hex, &this->hashes);
            }
        }
    }
};