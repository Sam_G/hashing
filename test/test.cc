#include <gtest/gtest.h>

#include "sha256.h"
#include "sha512.h"
#include "test_utils.hh"

TEST(sha512, short_messages){
    NISTVectorParser tests("shabytetestvectors/SHA512ShortMsg.rsp", 128);

    for(size_t i = 0; i < tests.test_vec.size(); i++){
        std::vector<uint8_t> input_vec = tests.test_vec.at(i);
        std::vector<uint8_t> ref_vec = tests.hashes.at(i);

        uint8_t* output = (uint8_t*)malloc(sizeof(uint8_t) * 64);
        sha512(input_vec.data(), input_vec.size(), output);

        std::vector<uint8_t> output_vec(output, output + 64);
        EXPECT_EQ(output_vec, ref_vec);
        free(output);
    }
}

TEST(sha512, long_messages){
    NISTVectorParser tests("shabytetestvectors/SHA512LongMsg.rsp", 128);

    for(size_t i = 0; i < tests.test_vec.size(); i++){
        std::vector<uint8_t> input_vec = tests.test_vec.at(i);
        std::vector<uint8_t> ref_vec = tests.hashes.at(i);

        uint8_t* output = (uint8_t*)malloc(sizeof(uint8_t) * 64);
        sha512(input_vec.data(), input_vec.size(), output);

        std::vector<uint8_t> output_vec(output, output + 64);
        EXPECT_EQ(output_vec, ref_vec);
        free(output);
    }
}

TEST(sha256, short_messages){
    NISTVectorParser tests("shabytetestvectors/SHA256ShortMsg.rsp", 64);

    for(size_t i = 0; i < tests.test_vec.size(); i++){
        std::vector<uint8_t> input_vec = tests.test_vec.at(i);
        std::vector<uint8_t> ref_vec = tests.hashes.at(i);

        uint8_t* output = (uint8_t*)malloc(sizeof(uint8_t) * 32);
        sha256(input_vec.data(), input_vec.size(), output);

        std::vector<uint8_t> output_vec(output, output + 32);
        EXPECT_EQ(output_vec, ref_vec);
        free(output);
    }
}

TEST(sha256, long_messages){
    NISTVectorParser tests("shabytetestvectors/SHA256LongMsg.rsp", 64);

    for(size_t i = 0; i < tests.test_vec.size(); i++){
        std::vector<uint8_t> input_vec = tests.test_vec.at(i);
        std::vector<uint8_t> ref_vec = tests.hashes.at(i);

        uint8_t* output = (uint8_t*)malloc(sizeof(uint8_t) * 32);
        sha256(input_vec.data(), input_vec.size(), output);

        std::vector<uint8_t> output_vec(output, output + 32);
        EXPECT_EQ(output_vec, ref_vec);
        free(output);
    }
}

int main(){
    testing::InitGoogleTest();
    return RUN_ALL_TESTS();
}