#include <stdlib.h>
#include <stdint.h>

#define uint128_t __uint128_t

#define ROTL_512(a,b) (((a) << (b)) | ((a) >> (64-(b))))
#define ROTR_512(a,b) (((a) >> (b)) | ((a) << (64-(b))))

#define EP0_512(x) (ROTR_512(x,28) ^ ROTR_512(x,34) ^ ROTR_512(x,39))
#define EP1_512(x) (ROTR_512(x,14) ^ ROTR_512(x,18) ^ ROTR_512(x,41))
#define SIG0_512(x) (ROTR_512(x,1) ^ ROTR_512(x,8) ^ ((x) >> 7))
#define SIG1_512(x) (ROTR_512(x,19) ^ ROTR_512(x,61) ^ ((x) >> 6))
#define CHOICE_512(x,y,z) (((x) & (y)) ^ ((~x) & (z)))
#define MAJOR_512(x,y,z) (((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)))

#ifdef __cplusplus
extern "C" {
#endif
void sha512(uint8_t* message, size_t len, uint8_t* buf);
#ifdef __cplusplus
}
#endif
