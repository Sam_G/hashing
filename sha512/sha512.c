#include "sha512.h"

#ifdef __cplusplus
extern "C" {
#endif
struct Message{
    uint8_t *message;
    size_t len;
};

static struct Message* prepare_message(uint8_t* message, size_t len){
    
    const uint128_t len_bits = (uint128_t) len * 8;
    size_t zero_pad_len = 1023 - 8 * ((len + 16) % 128);
           
    const size_t output_len_bits = len_bits + 1 + zero_pad_len + 128;
    struct Message* output = (struct Message*) malloc(sizeof(struct Message));

    if(output == NULL){
        return NULL;
    }
    
    output->len = output_len_bits / 8;
    output->message = (uint8_t*) malloc(sizeof(uint8_t) * output->len);
    
    if(output->message == NULL){
        free(output);
        return NULL;
    }

    // Copy contents of message to output
    for(unsigned long i = 0; i < len; i++){
        *(output->message + i) = *(message + i);
    }

    // Append  '1' to the output
    *(output->message + len) |= 1UL << 7;

    if(zero_pad_len <= 7){
        for(unsigned long i = 0; i < zero_pad_len; i++){
            *(output->message + len) &= ~(1UL << i);
        }
    } else {
        //Fill up first byte
        for(unsigned long i = 0; i < 7; i++){
            *(output->message + len) &= ~(1UL << i);
        }

        for(unsigned long i = 0; i < (zero_pad_len - 7)/8; i++){
            *(output->message + len + 1 + i) = 0x0;
        }

        for(unsigned long i = 0; i < (zero_pad_len - 7) % 8; i++){
            *(output->message + len + zero_pad_len/8) &= ~(1UL << i);
        }
    }

    uint8_t *len_bits_ptr = (uint8_t*)(&len_bits);
    
    for(int i = 0; i < 16; i++){
        *(output->message + output->len - 16 + i) = *(len_bits_ptr + 15 - i);
    }
    
    return output;
}

void sha512(uint8_t* message, size_t len, uint8_t* buf){

    // Constants [4.2.3]
    const uint64_t K[80] = 
    {0x428a2f98d728ae22, 0x7137449123ef65cd, 0xb5c0fbcfec4d3b2f, 0xe9b5dba58189dbbc,
    0x3956c25bf348b538, 0x59f111f1b605d019, 0x923f82a4af194f9b, 0xab1c5ed5da6d8118,
    0xd807aa98a3030242, 0x12835b0145706fbe, 0x243185be4ee4b28c, 0x550c7dc3d5ffb4e2,
    0x72be5d74f27b896f, 0x80deb1fe3b1696b1, 0x9bdc06a725c71235, 0xc19bf174cf692694,
    0xe49b69c19ef14ad2, 0xefbe4786384f25e3, 0x0fc19dc68b8cd5b5, 0x240ca1cc77ac9c65,
    0x2de92c6f592b0275, 0x4a7484aa6ea6e483, 0x5cb0a9dcbd41fbd4, 0x76f988da831153b5,
    0x983e5152ee66dfab, 0xa831c66d2db43210, 0xb00327c898fb213f, 0xbf597fc7beef0ee4,
    0xc6e00bf33da88fc2, 0xd5a79147930aa725, 0x06ca6351e003826f, 0x142929670a0e6e70,
    0x27b70a8546d22ffc, 0x2e1b21385c26c926, 0x4d2c6dfc5ac42aed, 0x53380d139d95b3df,
    0x650a73548baf63de, 0x766a0abb3c77b2a8, 0x81c2c92e47edaee6, 0x92722c851482353b,
    0xa2bfe8a14cf10364, 0xa81a664bbc423001, 0xc24b8b70d0f89791, 0xc76c51a30654be30,
    0xd192e819d6ef5218, 0xd69906245565a910, 0xf40e35855771202a, 0x106aa07032bbd1b8,
    0x19a4c116b8d2d0c8, 0x1e376c085141ab53, 0x2748774cdf8eeb99, 0x34b0bcb5e19b48a8,
    0x391c0cb3c5c95a63, 0x4ed8aa4ae3418acb, 0x5b9cca4f7763e373, 0x682e6ff3d6b2b8a3,
    0x748f82ee5defb2fc, 0x78a5636f43172f60, 0x84c87814a1f0ab72, 0x8cc702081a6439ec,
    0x90befffa23631e28, 0xa4506cebde82bde9, 0xbef9a3f7b2c67915, 0xc67178f2e372532b,
    0xca273eceea26619c, 0xd186b8c721c0c207, 0xeada7dd6cde0eb1e, 0xf57d4f7fee6ed178,
    0x06f067aa72176fba, 0x0a637dc5a2c898a6, 0x113f9804bef90dae, 0x1b710b35131c471b,
    0x28db77f523047d84, 0x32caab7b40c72493, 0x3c9ebe0a15c9bebc, 0x431d67c49c100d4c,
    0x4cc5d4becb3e42b6, 0x597f299cfc657e2a, 0x5fcb6fab3ad6faec, 0x6c44198c4a475817};

    // Initial Hash Value [5.2.5]
    uint64_t H[8] =
    {0x6a09e667f3bcc908, 0xbb67ae8584caa73b, 0x3c6ef372fe94f82b, 0xa54ff53a5f1d36f1, 
    0x510e527fade682d1, 0x9b05688c2b3e6c1f, 0x1f83d9abfb41bd6b, 0x5be0cd19137e2179};

    struct Message* M = prepare_message(message, len);
    
    if(M == NULL){
        // Need to improve error handling here!
        return;
    }

    for(unsigned long i = 0; i < M->len / 128; i++){
        uint8_t *Mi = (M->message + 128*i);
        uint64_t W[80];

        for(int t = 0, j = 0; t < 16; t++, j += 8){
            uint64_t p0 = (uint64_t)*(Mi+j);
            uint64_t p1 = (uint64_t)*(Mi+j + 1);
            uint64_t p2 = (uint64_t)*(Mi+j + 2);
            uint64_t p3 = (uint64_t)*(Mi+j + 3);
            uint64_t p4 = (uint64_t)*(Mi+j + 4);
            uint64_t p5 = (uint64_t)*(Mi+j + 5);
            uint64_t p6 = (uint64_t)*(Mi+j + 6);
            uint64_t p7 = (uint64_t)*(Mi+j + 7);
            
            W[t] =  (p0 << 56) | (p1 << 48) | (p2 << 40) | (p3 << 32) |
                    (p4 << 24) | (p5 << 16) | (p6 << 8) | (p7);
        }
        
        for(int t = 16; t < 80; t++){
            W[t] = (SIG1_512(W[t-2]) + W[t-7] + SIG0_512(W[t-15]) + W[t-16]);
        }

        uint64_t a = H[0], b = H[1], c = H[2], d = H[3], e = H[4], f = H[5], g = H[6], h = H[7];

        for(int t = 0; t < 80; t++){
            const uint64_t T1 = (h + EP1_512(e) + CHOICE_512(e,f,g) + K[t] + W[t]);
            const uint64_t T2 = (EP0_512(a) + MAJOR_512(a,b,c));
            
            h = g;
            g = f;
            f = e;
            e = (d + T1);
            d = c;
            c = b;
            b = a;
            a = (T1 + T2);
        }

        H[0] = (a + H[0]);
        H[1] = (b + H[1]);
        H[2] = (c + H[2]);
        H[3] = (d + H[3]);
        H[4] = (e + H[4]);
        H[5] = (f + H[5]);
        H[6] = (g + H[6]);
        H[7] = (h + H[7]);
    }
 
    for(int i = 0, j = 0; i < 8; i++){
        *(buf + j++) = (uint8_t)(H[i] >> 56);
        *(buf + j++) = (uint8_t)(H[i] >> 48);
        *(buf + j++) = (uint8_t)(H[i] >> 40);
        *(buf + j++) = (uint8_t)(H[i] >> 32);        
        *(buf + j++) = (uint8_t)(H[i] >> 24);
        *(buf + j++) = (uint8_t)(H[i] >> 16);
        *(buf + j++) = (uint8_t)(H[i] >> 8);
        *(buf + j++) = (uint8_t)(H[i]);
    }
    free(M->message);
    free(M);
}

#ifdef __cplusplus
}
#endif
